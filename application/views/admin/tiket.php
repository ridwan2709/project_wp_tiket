        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Tiket</h1>
          <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#newTiket">Tambah Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Film</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                    foreach($tiket as $t){
                        ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $t->harga ?></td>
                        <td><?= $t->stok ?></td>
                        <td><?= $t->judul?></td>
                        <td>
                            <a href="#" class="btn btn-success mr-1" data-toggle="modal" data-target="#ubahTiket<?= $t->id_tiket ?>"><span class="icon"><i class="fas fa-edit"></i></span></a>
                            <a href="<?= base_url() ?>admin/hapus_tiket/<?= $t->id_tiket ?>" class="btn btn-danger" onclick="alert('Ingin menghapus?')"><span class="icon"><i class="fas fa-trash"></i></span></a>
                        </td>
                    </tr>
                      <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <!-- Modal -->
<div class="modal fade" id="newTiket" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Tambah Data Tiket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/tiket'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="harga">Harga Tiket</label>
                        <input type="text" class="form-control" id="harga" name='harga'>
                    </div>
                    <div class="form-group">
                        <label for="stok">Stok Tiket</label>
                        <input class="form-control" type="text" name="stok" id="stok">
                    </div>
                    <div class="form-group">
                      <label for="film">Film</label>
                      <select name="film" id="film" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $film = $this->db->get('film')->result_array();
                          foreach($film as $fil):
                        ?>
                          <option value="<?= $fil['id_film'];?>"><?= $fil['judul']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php foreach ($tiket as $t ) {?>
        <!-- Modal -->
<div class="modal fade" id="ubahTiket<?= $t->id_tiket?>" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Ubah Data Tiket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/ubah_tiket/').$t->id_tiket; ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="harga">Harga Tiket</label>
                        <input type="text" class="form-control" id="harga" name='harga' value="<?= $t->harga ?>">
                    </div>
                    <div class="form-group">
                        <label for="stok">Stok Tiket</label>
                        <input class="form-control" type="text" name="stok" id="stok" value="<?= $t->stok ?>">
                    </div>
                    <div class="form-group">
                      <label for="film">Film</label>
                      <select name="film" id="film" class="custom-select">
                        <option disabled value="">Pilih...</option>
                        <?php 
                          $film = $this->db->get('film')->result_array();
                          foreach($film as $fil):
                        ?>
                          <option <?= $t->id_film == $fil['id_film'] ?'selected':'' ?>  value="<?= $fil['id_film'];?>"><?= $fil['judul']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
                          <?php } ?>