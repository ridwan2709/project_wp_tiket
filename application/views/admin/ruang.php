        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Ruang</h1>
          <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#newRuang">Tambah Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Ruang</th>
                        <th>Jumlah Kursi</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                    foreach($ruang as $r){
                        ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $r->nama ?></td>
                        <td><?= $r->jm_kursi ?></td>
                        <td>
                            <a href="#" class="btn btn-success mr-1" data-toggle="modal" data-target="#ubahRuang<?= $r->id_ruang ?>"><span class="icon"><i class="fas fa-edit"></i></span></a>
                            <a href="<?= base_url() ?>admin/hapus_ruang/<?= $r->id_ruang ?>" class="btn btn-danger" onclick="alert('Ingin menghapus?')"><span class="icon"><i class="fas fa-trash"></i></span></a>
                        </td>
                    </tr>
                      <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <!-- Modal -->
<div class="modal fade" id="newRuang" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Tambah Data Ruang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/ruang'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="nama">Nama Ruang</label>
                        <input type="text" class="form-control" id="nama" name='nama'>
                    </div>
                    <div class="form-group">
                        <label for="jm_kursi">Jumlah Kursi</label>
                        <input class="form-control" type="number" name="jm_kursi" id="jm_kursi">
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php foreach ($ruang as $r) { ?>
<!-- Modal -->
<div class="modal fade" id="ubahRuang<?= $r->id_ruang ?>" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Ubah Data Ruang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/ubah_ruang/').$r->id_ruang; ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="nama">Nama Ruang</label>
                        <input type="text" class="form-control" id="nama" name='nama' value="<?= $r->nama ?>">
                    </div>
                    <div class="form-group">
                        <label for="jm_kursi">Jumlah Kursi</label>
                        <input class="form-control" type="number" name="jm_kursi" id="jm_kursi" value="<?= $r->jm_kursi ?>">
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php } ?>