        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Jadwal Film</h1>
          <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#newRuang">Tambah Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Film </th>
                        <th>Tanggal Tayang</th>
                        <th>Tanggal Berhenti</th>
                        <th>Sesi</th>
                        <th>Ruang</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                    foreach($jadwal as $j){
                        $film = $this->db->get_where('film', array('id_film' => $j->id_film))->result_array();
                        $sesi = $this->db->get_where('sesi', array('id_sesi' => $j->id_sesi))->result_array();
                        $ruang = $this->db->get_where('ruang', array('id_ruang' => $j->id_ruang))->result_array();
                        ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $film[0]['judul'] ?></td>
                        <td><?= $j->tgl_mulai?></td>
                        <td><?= $j->tgl_berhenti?></td>
                        <td><?= $sesi[0]['sesi']?></td>
                        <td><?= $ruang[0]['nama']?></td>
                        <td>
                            <a href="#" class="btn btn-success mr-1" data-toggle="modal" data-target="#ubahJadwal<?= $j->id_jadwal ?>"><span class="icon"><i class="fas fa-edit"></i></span></a>
                            <a href="<?= base_url() ?>admin/hapus_jadwal/<?= $j->id_jadwal ?>" class="btn btn-danger" onclick="alert('Ingin menghapus?')"><span class="icon"><i class="fas fa-trash"></i></span></a>
                        </td>
                    </tr>
                      <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <!-- Modal -->
<div class="modal fade" id="newRuang" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Tambah Data Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/jadwal'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="film">Film</label>
                      <select name="id_film" id="film" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $film = $this->db->get('film')->result_array();
                          foreach($film as $fil):
                        ?>
                          <option value="<?= $fil['id_film'];?>"><?= $fil['judul']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="form-group">
                        <label for="tgl_mulai">Tanggal Tayang</label>
                        <input class="form-control" type="date" name="tgl_mulai" id="tgl_mulai">
                    </div>
                    <div class="form-group">
                        <label for="tgl_berhenti">Tanggal Berhenti</label>
                        <input class="form-control" type="date" name="tgl_berhenti" id="tgl_berhenti">
                    </div>
                    <div class="form-group">
                      <label for="sesi">Sesi</label>
                      <select name="id_sesi" id="sesi" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $sesi = $this->db->get('sesi')->result_array();
                          foreach($sesi as $ses):
                        ?>
                          <option value="<?= $ses['id_sesi'];?>"><?= $ses['sesi']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="ruang">Ruang</label>
                      <select name="id_ruang" id="ruang" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $ruang = $this->db->get('ruang')->result_array();
                          foreach($ruang as $rua):
                        ?>
                          <option value="<?= $rua['id_ruang'];?>"><?= $rua['nama']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php foreach ($jadwal as $j) { ?>
        <!-- Modal -->
<div class="modal fade" id="ubahJadwal<?= $j->id_jadwal ?>" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Ubah Data Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/jadwal/').$j->id_jadwal; ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="film">Film</label>
                      <select name="id_film" id="film" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $film = $this->db->get('film')->result_array();
                          foreach($film as $fil):
                        ?>
                          <option <?= $j->id_film == $fil['id_film'] ?'selected':'' ?> value="<?= $fil['id_film'];?>"><?= $fil['judul']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="form-group">
                        <label for="tgl_mulai">Tanggal Tayang</label>
                        <input class="form-control" type="date" name="tgl_mulai" id="tgl_mulai" value="<?= $j->tgl_mulai ?>">
                    </div>
                    <div class="form-group">
                        <label for="tgl_berhenti">Tanggal Berhenti</label>
                        <input class="form-control" type="date" name="tgl_berhenti" id="tgl_berhenti" value="<?= $j->tgl_berhenti ?>">
                    </div>
                    <div class="form-group">
                      <label for="sesi">Sesi</label>
                      <select name="id_sesi" id="sesi" class="custom-select">
                        <option disabled value="">Pilih...</option>
                        <?php 
                          $sesi = $this->db->get('sesi')->result_array();
                          foreach($sesi as $ses):
                        ?>
                          <option <?= $j->id_sesi == $ses['id_sesi'] ?'selected':'' ?> value="<?= $ses['id_sesi'];?>"><?= $ses['sesi']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="ruang">Ruang</label>
                      <select name="id_ruang" id="ruang" class="custom-select">
                        <option disabled value="">Pilih...</option>
                        <?php 
                          $ruang = $this->db->get('ruang')->result_array();
                          foreach($ruang as $rua):
                        ?>
                          <option <?= $j->id_ruang == $rua['id_ruang'] ?'selected':'' ?> value="<?= $rua['id_ruang'];?>"><?= $rua['nama']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>

                          <?php } ?>