        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Member</h1>
          <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#newMember">Tambah Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>tanggal Lahir</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                      foreach($member as $m){
                    ?>
                    <tr>
                        <td><?=$i ?></td>
                        <td><?= $m->nama ?></td>
                        <td><?= $m->email ?></td>
                        <td><?= $m->jk ?></td>
                        <td><?= $m->tgl_lahir ?></td>
                        <td><img src="<?= base_url('assets/')?>img/member/<?=$m->foto ?>.jpg" alt="" width="100"></td>
                        <td>
                            <a href="#" class="btn btn-success mr-1" data-toggle="modal" data-target="#ubahMember<?= $m->id_member; ?>"><span class="icon"><i class="fas fa-edit"></i></span></a>
                            <a href="<?= base_url() ?>admin/hapus_member/<?= $m->id_member ?>" class="btn btn-danger" onclick="alert('Ingin menghapus?')"><span class="icon"><i class="fas fa-trash"></i></span></a>
                        </td>
                    </tr>
                      <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <!-- Modal -->
<div class="modal fade" id="newMember" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Tambah Data Member</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/member'); ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="nama">Nama Member</label>
                        <input type="text" class="form-control" id="nama" name='nama'>
                    </div>
                    <div class="form-group">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input class="form-control" type="text" name="tgl_lahir" id="tgl_lahir" placeholder="yyy-mm-dd">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name='email'>
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" name="password" id="password" >
                    </div>
                    <div class="form-group">
                      <label for="jk">Jenis Kelamin</label>
                        <select name="jk" id="jk" class="form-control">
                            <option value="" class="form-control">Pilih...</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-bottom:-15px;">
                      <label for="foto">Foto</label>
                      <input type="file" name="foto" id="foto" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php foreach($member as $m): ?>
<!-- Modal -->
<div class="modal fade" id="ubahMember<?= $m->id_member ?>" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Ubah Data Member</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/ubah_member/').$m->id_member; ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="nama">Nama Member</label>
                        <input type="text" class="form-control" id="nama" name='nama' value="<?= $m->nama ?>">
                    </div>
                    <div class="form-group">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input class="form-control" type="text" name="tgl_lahir" id="tgl_lahir" placeholder="yyy-mm-dd" value="<?= $m->tgl_lahir ?>">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name='email' value="<?= $m->email ?>">
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" name="password" id="password"  required>
                    </div>
                    <div class="form-group">
                      <label for="jk">Jenis Kelamin</label>
                      <?php if ($m->jk == 'Laki-Laki') {
                          $laki = 'selected';
                          $perem = '';
                        }else {
                            $laki = '';
                            $perem = 'selected';
                      }
                       ?>
                        <select name="jk" id="jk" class="form-control">
                            <option value="" >Pilih...</option>
                            <option <?= $laki ?> value="Laki-Laki">Laki-Laki</option>
                            <option <?= $perem ?> value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-bottom:-15px;">
                      <label for="foto">Foto</label>
                      <input type="file" name="foto" id="foto" class="form-control" value="<?= $m->foto ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>