        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Pemesan</h1>
          <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#newRuang">Tambah Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>ID Pesanan </th>
                        <th>Nama</th>
                        <th>Jumlah Tiket</th>
                        <th>Harga Total</th>
                        <th>Tanggal Pesan</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                    foreach($pesanan as $p){
                        $member = $this->db->get_where('member', array('id_member' => $p->id_member))->result_array();
                        ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $p->id_pemesan?></td>
                        <td><?= $member[0]['nama'] ?></td>
                        <td><?= $p->jml_tiket_pesan?></td>
                        <td><?= $p->total_harga ?></td>
                        <td><?= $p->tgl_pesan ?></td>
                        <td class="<?= $p->status ? 'text-success' : 'text-warning' ?> "><?= $p->status ? 'Telah dibayar' : 'Belum dibayar' ?></td>
                        <td>
                            <a href="#" class="btn btn-primary mr-1" data-toggle="modal" data-target="#detailPesan<?= $p->id_pemesan ?>"><span class="icon"><i class="fas fa-eye"></i></span></a>
                            <a href="<?= base_url() ?>admin/hapus_pesanan/<?= $p->id_pemesan ?>" class="btn btn-danger" onclick="alert('Ingin menghapus?')"><span class="icon"><i class="fas fa-trash"></i></span></a>
                        </td>
                    </tr>
                      <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <!-- Modal -->
<div class="modal fade" id="newRuang" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Tambah Pesanan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/pesanan'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="film">Film</label>
                      <select name="id_film" id="film" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $film = $this->db->get('film')->result_array();
                          foreach($film as $fil):
                        ?>
                          <option value="<?= $fil['id_film'];?>"><?= $fil['judul']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="form-group">
                        <label for="tgl_mulai">Tanggal Tayang</label>
                        <input class="form-control" type="date" name="tgl_mulai" id="tgl_mulai">
                    </div>
                    <div class="form-group">
                        <label for="tgl_berhenti">Tanggal Berhenti</label>
                        <input class="form-control" type="date" name="tgl_berhenti" id="tgl_berhenti">
                    </div>
                    <div class="form-group">
                      <label for="sesi">Sesi</label>
                      <select name="id_sesi" id="sesi" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $sesi = $this->db->get('sesi')->result_array();
                          foreach($sesi as $ses):
                        ?>
                          <option value="<?= $ses['id_sesi'];?>"><?= $ses['sesi']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="ruang">Ruang</label>
                      <select name="id_ruang" id="ruang" class="custom-select">
                        <option selected disabled value="">Pilih...</option>
                        <?php 
                          $ruang = $this->db->get('ruang')->result_array();
                          foreach($ruang as $rua):
                        ?>
                          <option value="<?= $rua['id_ruang'];?>"><?= $rua['nama']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php foreach($pesanan as $p){ ?>
     <!-- Modal -->
     <div class="modal fade" id="detailPesan<?= $p->id_pemesan ?>" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Detail Pesanan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="table-responsive p-3">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Judul</th>
                        <th>Rilis</th>
                        <th>Genre</th>
                        <th>Kursi</th>
                        <th>Harga</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $query = $this->db->query("SELECT * FROM film,tiket,dtl_pemesan WHERE film.id_film = tiket.id_film AND tiket.id_tiket = dtl_pemesan.id_tiket AND dtl_pemesan.id_pemesan = '$p->id_pemesan'")->result_array();
                    $i=1;
                    foreach($query as $q){
                        ?>
                    <tr>
                      <td><?= $i; ?></td>
                      <td><?= $q['judul']; ?></td>
                      <td><?= $q['rilis']; ?></td>
                      <td><?= $q['genre']; ?></td>
                      <td><?= $q['kursi']; ?></td>
                      <td>Rp. <?= number_format($q['harga'],2,",","."); ?></td>
                    </tr>
                      <?php $i++; } ?>
                      <tr>
                        <th colspan="5">Jumlah</th>
                        <th colspan="5">Rp. <?= number_format($p->total_harga,2,",",".") ?></th>
                      </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <a href="<?= base_url('admin/proses_pesanan/'). $p->id_pemesan ?>" target="_blank" type="submit" class="btn btn-primary">Bayar</a>
                </div>
            </div>
    </div>
</div>

<?php } ?>