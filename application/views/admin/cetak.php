
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Cetak Tiket</title>
 </head>
 <body>
     <?php foreach ($data as $row) {;?>
 	<div class="box-tiket">
 		<div class="kepala-tiket">
 			<h1>Cinema 21</h1>
 		</div>
 		<div class="isi">
 			<h2><?= $row['judul'] ?></h2>
 			<table>
 				<tr>
 					<td>DATE</td>
 					<td>:</td>
 					<td id="font"><?= $row['tgl_mulai'] ?></td>
 				</tr>
 				<tr>
 					<td>TIME</td>
 					<td>:</td>
 					<td id="font"><?= $row['mulai'] ?></td>
 				</tr>
 				<tr>
 					<td>KURSI</td>
 					<td>:</td>
 					<td id="font"><?= $row['kursi'] ?></td>
 				</tr>
 				<tr>
 					<td>HARGA</td>
 					<td>:</td>
 					<td> Rp.<?= number_format($row['harga'],0,'.','.') ?></td>
 				</tr>
 			</table>	
 		</div>
 		<div class="sesi">
 			<h1><?= $row['sesi'] ?></h1>
 		</div>
     </div>
     <?php } ?>
 </body>
 </html>
 <style type="text/css">
 	.box-tiket{
 		width: 400px;
 		height: 350px;
 		color: #4c4447;
 		font-family: lucida console;
 		background-color: #ebf3f4;
 	}
 	.kepala-tiket{
 		width: 100%;
 		height: 100px;
 		text-align: center;
 		font-size: 18px;
 		border-bottom: 3px solid #4c4447;
 	}
 	.kepala-tiket h1{
 		padding: 15px;
 	}
 	.isi{
 		padding: 20px;
 		float: left;
 	}
 	.sesi{
 		float: left;
 		padding: 0px;
 		margin:0px;
 	}
 	.sesi h1{
 		font-size: 130px;
 	}
 	#font{
 		font-size: 25px;
 		font-weight: bold;
 	}
 </style>
 <script type="text/javascript">
 	window.load=b_print();
	function b_print(){
		window.print();
	};
 </script>