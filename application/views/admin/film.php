        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Film</h1>
          <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#newFilm">Tambah Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Judul</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Durasi</th>
                        <th>Rilis</th>
                        <th>Score</th>
                        <th>Sinopsis</th>
                        <th>Gambar</th>
                        <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                      foreach($film as $f){
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $f->judul ?></td>
                        <td><?= $f->genre ?></td>
                        <td><?= $f->rating ?></td>
                        <td><?= $f->durasi ?></td>
                        <td><?= $f->rilis ?></td>
                        <td><?= $f->score ?></td>
                        <td><?= $f->sinopsis ?></td>
                        <td><img src="<?= base_url('assets/')?>img/film/<?=$f->gambar ?>.jpg" alt="" width="100"></td>
                        <td>
                            <a href="#" class="btn btn-success mr-1" data-toggle="modal" data-target="#ubahFilm<?= $f->id_film ?>"><span class="icon"><i class="fas fa-edit"></i></span></a>
                            <a href="<?= base_url() ?>admin/hapus_film/<?= $f->id_film ?>" class="btn btn-danger" onclick="alert('Ingin menghapus?')"><span class="icon"><i class="fas fa-trash"></i></span></a>
                        </td>
                    </tr>
                      <?php $i++; } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <!-- Modal -->
<div class="modal fade" id="newFilm" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Tambah Data Film</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/film'); ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="judul">Judul Film</label>
                        <input type="text" class="form-control" id="judul" name='judul'>
                    </div>
                    <div class="form-group">
                        <label for="genre">Genre</label>
                        <input class="form-control" type="text" name="genre" id="genre">
                    </div>
                    <div class="form-group">
                      <label for="durasi">Durasi (menit)</label>
                        <input type="text" class="form-control" id="durasi" name='durasi'>
                    </div>
                    <div class="form-group">
                        <label for="rating">Ratting</label>
                        <select name="rating" id="rating" class="form-control">
                            <option>Pilih...</option>
                            <option value="G">G</option>
                            <option value="PG">PG</option>
                            <option value="PG-13">PG-13</option>
                            <option value="R">R</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="score">Score</label>
                        <input type="score" class="form-control" name="score" id="score" >
                    </div>
                    <div class="form-group">
                        <label for="rilis">Tahun Rilis</label>
                        <input type="rilis" class="form-control" name="rilis" id="rilis" >
                    </div>
                    <div class="form-group">
                        <label for="sinopsis">Sinopsis</label>
                        <textarea  class="form-control" name="sinopsis" id="sinopsis"" rows="5"></textarea>
                    </div>
                    <div class="form-group" style="margin-bottom:-15px;">
                      <label for="foto">Gambar</label>
                      <input type="file" name="foto" id="foto" class="form-control-file">
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
  foreach ($film as $f) {
?> 
       <!-- Modal -->
<div class="modal fade" id="ubahFilm<?= $f->id_film ?>" tabindex="-1" role="dialog" aria-labelledby="newMemberLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMember">Ubah Data Film</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/ubah_film/').$f->id_film; ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                      <label for="judul">Judul Film</label>
                        <input type="text" class="form-control" id="judul" name='judul' value="<?= $f->judul ?>">
                    </div>
                    <div class="form-group">
                        <label for="genre">Genre</label>
                        <input class="form-control" type="text" name="genre" id="genre" value="<?= $f->genre ?>">
                    </div>
                    <div class="form-group">
                      <label for="durasi">Durasi (menit)</label>
                        <input type="text" class="form-control" id="durasi" name='durasi' value="<?= $f->durasi ?>">
                    </div>
                    <div class="form-group">
                        <label for="rating">Ratting</label>
                        <select name="rating" id="rating" class="form-control">
                            <option>Pilih...</option>
                            <option <?= $f->rating == 'G'?'selected':'' ?> value="G">G</option>
                            <option <?= $f->rating == 'PG'?'selected':'' ?> value="PG">PG</option>
                            <option <?= $f->rating == 'PG-13'?'selected':'' ?> value="PG-13">PG-13</option>
                            <option <?= $f->rating == 'R'?'selected':'' ?> value="R">R</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="score">Score</label>
                        <input type="score" class="form-control" name="score" id="score"  value="<?= $f->score ?>">
                    </div>
                    <div class="form-group">
                        <label for="rilis">Tahun Rilis</label>
                        <input type="rilis" class="form-control" name="rilis" id="rilis"  value="<?= $f->rilis ?>">
                    </div>
                    <div class="form-group">
                        <label for="sinopsis">Sinopsis</label>
                        <textarea  class="form-control" name="sinopsis" id="sinopsis"" rows="5"><?= $f->sinopsis ?></textarea>
                    </div>
                    <div class="form-group" style="margin-bottom:-15px;">
                      <label for="foto">Gambar</label>
                      <input type="file" name="foto" id="foto" class="form-control-file"  value="<?= $f->gambar ?>">
                    </div>
                </div><br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>

  <?php } ?>