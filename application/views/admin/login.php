<div class="container">
    <div class="row">
        <div class="col">
                <div class="card o-hidden border-0 shadow-lg my-2">
                    <h2 class="primary mt-2 ml-3" style="color: #365ccd;">Bioskop Cinema 21</h2>
                </div>
        </div>
    </div>
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-lg-7">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Login Page!</h1>
                                <?= $this->session->flashdata('message'); ?>
                                </div>
                                <form class="user" method="POST" action="<?= base_url('administrator'); ?>">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="username" placeholder="Enter Username..." name="username" ">
                                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" id="password" placeholder="Password" name="password">
                                        <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Login
                                    </button>
                                </form>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>