<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-film"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Cinema 21</div>
    </a>

    <!-- query menu -->
    <?php
    $this->db->order_by('id', 'asc');
    $menu = $this->db->get('menu')->result_array();
    ?>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- looping menu -->
        <div class="sidebar-heading">
            Administrator
        </div>
    <?php
    foreach ($menu as $m) :
    ?>
            <!-- Nav Item - Dashboard -->
            <?php if ($title == $m['title']) : ?>
                <li class="nav-item active">
                <?php else : ?>
                <li class="nav-item">
                <?php endif; ?>
                <a class="nav-link" href="<?= base_url($m['url']); ?>">
                    <i class="<?= $m['icon']; ?>"></i>
                    <span><?= $m['title'];  ?></span></a>
                </li>
            <!-- Divider -->
            <?php endforeach; ?>
            <hr class="sidebar-divider">
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('administrator/logout') ?>">
                <i class="fas fa-fw fa-sign-out-alt"></i>
                <span>Logout</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">


        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

</ul>
<!-- End of Sidebar -->