<?php 

$kode = $pesanan[0]['kode'];
$nu = (int) substr($kode, 4,5);
$nu++;

$char = "PMSN";
$newid = $char . sprintf("%05s",$nu);
if (!$user) {
	$log = '<a href="#" data-toggle="modal" data-target="#modalLogin" >Login</a>';
	$reg = '<a class="small" href="#" data-toggle="modal" data-target="#modalRegister">Register</a>';
}else{
	$log = '<a href="'. base_url().'auth/logout">Logout</a>';
	$reg = '<a href="#" data-toggle="modal" data-target="#modalKeranjang" >Keranjang</a>';
}
?>
<div id="demo-1" data-zs-src='["<?= base_url() ?>assets/img/2.jpg", "<?= base_url() ?>assets/img/1.jpg", "<?= base_url() ?>assets/img/3.jpg","<?= base_url() ?>assets/img/4.jpg"]' data-zs-overlay="dots">
		<div class="demo-inner-content">
		<!--/header-w3l-->
			   <div class="header-w3-agileits" id="home">
			     <div class="inner-header-agile">	
                                <?= $this->session->flashdata('message'); ?>
				<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a  href="<?= base_url() ?>"><span>C</span>inema <span>21</span></a></h1>
					</div>
					<!-- navbar-header -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
							<li class="active"><a href="index.html">Home</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Genre <b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns-3">
									<li>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="#">Action</a></li>
											<li><a href="#">Biography</a></li>
											<li><a href="#">Crime</a></li>
											<li><a href="#">Family</a></li>
											<li><a href="#">Horror</a></li>
											<li><a href="#">Romance</a></li>
											<li><a href="#">Sports</a></li>
											<li><a href="#">War</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="#">Adventure</a></li>
											<li><a href="#">Comedy</a></li>
											<li><a href="#">Documentary</a></li>
											<li><a href="#">Fantasy</a></li>
											<li><a href="#">Thriller</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="#">Animation</a></li>
											<li><a href="#">Costume</a></li>
											<li><a href="#">Drama</a></li>
											<li><a href="#">History</a></li>
											<li><a href="#">Musical</a></li>
											<li><a href="#">Psychological</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
									</li>
								</ul>
							</li>
							<li><?= $log ?></li>
							<li><?= $reg ?></li>
						</ul>

					</div>
					<div class="clearfix"> </div>	
				</nav>
					<div class="w3ls_search">
									<div class="cd-main-header">
										<ul class="cd-header-buttons">
											<li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
										</ul> <!-- cd-header-buttons -->
									</div>
									<div id="cd-search" class="cd-search">
										<form action="#" method="post">
											<input name="Search" type="search" placeholder="Search...">
										</form>
									</div>
								</div>
	
			</div> 

			   </div>
		<!--//header-w3l-->
			<!--/banner-info-->
			   <div class="baner-info">
			      <h3>Latest <span>On</span>Line <span>Mo</span>vies</h3>
				  <h4>In space no one can hear you scream.</h4>
			   </div>
			<!--/banner-ingo-->		
		</div>
    </div>
  <!--/banner-section-->
  <!-- Modal LOGIN -->
  <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" >

<div class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Login Page!</h1>
                                </div>
                                <form class="user" method="POST" action="<?= base_url('auth'); ?>">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" id="email" placeholder="Enter Email Address..." name="email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" id="password" placeholder="Password" name="password">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Login
                                    </button>
                                </form>
                                <hr>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
</div>
<!-- //Modal LOGIN -->

<!-- Modal REGISTER -->
<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" >

<div class="modal-dialog">
<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>Register</h4>
			<br><br>
				<form action="<?= base_url('auth/registration') ?>" method="post">
				<div class="form-group">
					<input class="form-control" type="text" name="nama" placeholder="Name">
				</div>
				<div class="form-group">
					<input class="form-control" type="email" name="email" placeholder="E-mail">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" name="tgl_lahir" id="tgl_lahir" placeholder="Date Birth">
				</div>
				<div class="form-group">
					<select name="jk" id="jk" class="form-control">
						<option value="" selected >Pilih..</option>
						<option value="Laki-Laki">Laki-Laki</option>
						<option value="Perempuan">Perempuan</option>
					</select>
				</div>
				<div class="form-group">
					<input class="form-control" type="password" name="pass1" placeholder="Password">
				</div>
				<div class="form-group">
					<input class="form-control" type="password" name="pass2" placeholder="Repeat Password">
				</div>
					<div class="tp">
						<button class="btn btn-primary" type="submit">Register Now</button>
					</div>
				</form>
		</div>
	</div>
</div>
</div>
<!-- //Modal REGISTER -->
					<div class="modal fade" id="modalKeranjang" tabindex="-1" role="dialog" >
							<div class="modal-dialog" style="width: 80%;">
							<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4>Daftar Blanja</h4>
										<div class="login-form">
											<form action="<?= base_url('welcome') ?>/proses_pesanan" method="post">
												<table class="table">
												<?php
												$tiket = $this->db->query("SELECT COUNT(*) FROM dtl_pemesan WHERE id_pemesan = '$newid'")->row_array();
												$jm_tiket = $tiket["COUNT(*)"];

												$total_harga = $this->db->query("SELECT SUM(tiket.harga) as total_harga FROM tiket, dtl_pemesan WHERE tiket.id_tiket = dtl_pemesan.id_tiket AND dtl_pemesan.id_pemesan = '$newid'")->result_array();

												 	$no = "1";

													$query = $this->db->query("SELECT * FROM film,tiket,dtl_pemesan WHERE film.id_film = tiket.id_film AND tiket.id_tiket = dtl_pemesan.id_tiket AND dtl_pemesan.id_pemesan = '$newid'")->result_array();
													foreach ($query as $dt2) {
												 ?>
													<tr>
														<td><?php echo $no++; ?></td>
														<td><?php echo $dt2['judul']; ?></td>
														<td><?php echo $dt2['rilis']; ?></td>
														<td><?php echo $dt2['genre']; ?></td>
														<td>No Kursi : <?php echo $dt2['kursi']; ?></td>
														<td>Rp. <?php echo number_format($dt2['harga'],2,",","."); ?></td>
														<td><a href="<?= base_url('welcome') ?>/hapus_pesan/<?php echo $dt2['id_dtl_pemesan']; ?>"><span class="label label-danger">DELET</span></a></td>
													</tr>
													
												<?php } ?>
												</table>
													<input type="hidden" name="id_pemesan" value="<?php echo $newid; ?>">
													<input type="hidden" name="id_member" value="<?= $user['id_member']; ?>">
													<input type="hidden" name="jm_tiket" value="<?= $jm_tiket; ?>">
													<input type="hidden" name="t_harga" value="<?php echo $total_harga[0]['total_harga']; ?>">
												<div class="tp" style="width: 100px;">
													<button class="btn btn-primary">Pesan</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>