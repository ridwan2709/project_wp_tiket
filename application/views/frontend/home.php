<h3 class="agile_w3_title" style="text-align: center;"> Top <span>Movies</span></h3>
	<!--/movies-->				
	<div class="w3_agile_latest_movies">
		<div id="owl-demo" class="owl-carousel owl-theme">

		<?php
        $this->db->order_by('score', 'desc');
        $sql = $this->db->get('film')->result_array();
			foreach ($sql as $data) {
 		?>	
		<div class="item">
			<div class="w3l-movie-gride-agile w3l-movie-gride-slider ">
				<a href="<?= base_url('welcome/detail_film/'. $data['id_film']) ?>" class="hvr-sweep-to-bottom"><img src="<?= base_url() ?>assets/img/film/<?php echo $data['gambar'] ?>.jpg" title="<?php echo $data['judul'] ?>" class="img-responsive" style="height: 300px;width: 300px;" alt=" " />
					<div class="w3l-action-icon"><i class="fa fa-play-circle-o" aria-hidden="true"></i></div>
				</a>
				<div class="mid-1 agileits_w3layouts_mid_1_home">
					<div class="w3l-movie-text">
						<h6><a href="views/info_film.php?id=<?php echo $data['id_film']; ?>"><?php echo $data['judul'] ?></a></h6>							
					</div>
					<div class="mid-2 agile_mid_2_home">
						<p><?php echo $data['rilis']; ?></p>
						<p style="margin-left: 58px;"><?php echo $data['rating']; ?></p>
						<p style="float: right;color: red;font-weight: bold;"><?php echo $data['score']; ?></p>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<!--//movies-->
	</div>	
</div>

 <h3 class="agile_w3_title" style="text-align: center;">ALL <span>Movies</span> </h3>
				<!--/requested-movies-->
<div class="wthree_agile-requested-movies">
	<?php
		$qr = $this->db->get('film')->result_array();
		foreach ($qr as $dt) {
 	?>	
	<div class="col-md-2 w3l-movie-gride-agile requested-movies">
		<a href="<?= base_url('welcome/detail_film/'. $dt['id_film']) ?>" class="hvr-sweep-to-bottom"><img src="<?= base_url() ?>assets/img/film/<?= $dt['gambar']; ?>.jpg" class="img-responsive" title="<?php echo $dt['judul'] ?>" style="height: 250px;width: 260px;" alt=" ">
			<div class="w3l-action-icon"><i class="fa fa-play-circle-o" aria-hidden="true"></i></div>
		</a>
		<div class="mid-1 agileits_w3layouts_mid_1_home">
			<div class="w3l-movie-text">
				<h6><a href="views/info_film.php?id=<?php echo $dt['id_film']; ?>"><?php echo $dt['judul'] ?></a></h6>							
			</div>
			<div class="mid-2 agile_mid_2_home">
				<p><?php echo $dt['rilis']; ?></p>
				<p style="margin-left: 58px;"><?php echo $dt['rating']; ?></p>
				<p style="float: right;color: red;font-weight: bold;"><?php echo $dt['score']; ?></p>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="clearfix"></div>
</div>
				<!--//requested-movies-->