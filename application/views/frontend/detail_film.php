                <?php 
                $kode = $pesanan[0]['kode'];
                $nu = (int) substr($kode, 4,5);
                $nu++;
                $char = "PMSN";
                $newid = $char . sprintf("%05s",$nu);
                if (!$user) {
                    $log = '<a class="btn btn-primary href="#" data-toggle="modal" data-target="#modalLogin" >Login</a>';
                }else{
                    $log = '<button class="buy btn-primary" data-toggle="modal" data-target="#add" >ADD</button>';
                }
foreach ($jadwal as $j):
?>
<div class="w3_content_agilleinfo_inner">
	<div class="agile_featured_movies">
		<div class="latest-news-agile-info">
			<div class="col-md-8 latest-news-agile-left-content">
				<div class="single video_agile_player">
					<div class="video-grid-single-page-agileits" style="float: left;">
					<img src="<?= base_url() ?>assets/img/film/<?php echo $tampil[0]['gambar'] ?>.jpg" alt="" class="img-responsive" style="width: 300px;height: 400px;" title="<?php echo $tampil[0]['judul']; ?>" />
					</div>
						<div class="info">
							<h4 style="font-size: 30px; margin: 0px;"><?php echo $tampil[0]['judul']; ?></h4>
							<p style="padding: 4px;border:1px solid #eee;font-size: 12px;">Genre : <?php echo $tampil[0]['genre']; ?> | Rating : <?php echo $tampil[0]['rating']; ?> | Score : <?php echo $tampil[0]['score']; ?></p>
							<p class="bar-kusus"><?php echo substr($tampil[0]['durasi'],0,5); ?> Menit | Rp.<?php echo number_format($tampil[0]['harga'],2,",","."); ?></p>
							<p class="bar-kusus"><?php echo date('d F Y', strtotime($j['tgl_mulai'])); ?> - <?php echo date('d F Y', strtotime($j['tgl_berhenti'])); ?></p>
							<fieldset>
								<legend>Summary</legend>
								<p><?php echo $tampil[0]['sinopsis']; ?></p>
							</fieldset>
							<?= $log ?>
						</div>
				</div>
			</div>
		</div>
			<div class="clearfix"> </div>
	</div>
</div>
<!--//content-inner-section-->
<?php endforeach; ?>
                <!-- modal add -->
                <div class="modal fade" id="add" tabindex="-1" role="dialog" >
					<div class="modal-dialog">
					<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4>Pilih Nomor Kursi</h4>
								<br>
								<div class="form-group">
									<form action="<?= base_url() ?>welcome/pesanan" method="post">
										<input type="hidden" name="tiket" value="<?php echo $tampil[0]['id_tiket']; ?>">
										<input type="hidden" name="pemesan" value="<?php echo $newid; ?>">
										<input required type="number" name="kursi" placeholder="Pilih No Kursi" min="0" max="<?php echo $tampil[0]['jm_kursi']; ?>" class="form-control">
										<br>
										<button class="btn btn-primary" type="submit">Add</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>