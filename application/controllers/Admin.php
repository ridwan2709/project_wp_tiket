<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$user = $this->session->userdata('username');
		if (!$user) {
			redirect('/');
		}
	}

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('username')])->row_array();

		

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('templates/footer');
	}
	
	public function member()
    { 
        $data['title'] = 'Data Member';
        $data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
		$this->db->order_by('id_member', 'desc');
		$data['member'] = $this->db->get('member')->result();
        
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/member', $data);
            $this->load->view('templates/footer');
        } else {
			$data = [
			'nama'      => $this->input->post('nama'),
			'email'      => $this->input->post('email'),
			'password'  => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'jk'     => $this->input->post('jk'),
			'tgl_lahir'     => $this->input->post('tgl_lahir'),
			'foto'       => substr(md5(rand(100000000, 200000000)), 0, 10)
			];
			$this->db->insert('member', $data);
			$foto = $this->db->get_where('member' , array('id_member' => $this->db->insert_id()))->row()->foto;
			move_uploaded_file($_FILES['foto']['tmp_name'], 'assets/img/member/' . $foto . '.jpg');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Member berhasil ditambahkan </div>');
            redirect('admin/member');
        }
	}public function ubah_member($id){
        
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Member gagal diubah</div>');
            redirect('admin/member');
        } else {
            $data = [
				'nama'      => $this->input->post('nama'),
				'email'      => $this->input->post('email'),
				'password'  => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'jk'     => $this->input->post('jk'),
				'tgl_lahir'     => $this->input->post('tgl_lahir'),
				'foto'       => substr(md5(rand(100000000, 200000000)), 0, 10)
            ];
            $this->db->where('id_member', $id);
            $this->db->update('member',$data);
			$foto = $data['foto'];
			move_uploaded_file($_FILES['foto']['tmp_name'], 'assets/img/member/' . $foto . '.jpg');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Member berhasil diubah </div>');
            redirect('admin/member');
        }
    }
	
	public function hapus_member($id='')
	{
		$this->db->where('id_member', $id);
		$this->db->delete('member');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Member berhasil dihapus </div>');
		redirect('admin/member');
	}
	public function film()
    { 
        $data['title'] = 'Data Film';
        $data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
		$this->db->order_by('id_film', 'desc');
		$data['film'] = $this->db->get('film')->result();
        
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('genre', 'Genre', 'required');
        $this->form_validation->set_rules('rating', 'rating', 'required');
        $this->form_validation->set_rules('durasi', 'durasi', 'required');
        $this->form_validation->set_rules('score', 'score', 'required');
        $this->form_validation->set_rules('rilis', 'rilis', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/film', $data);
            $this->load->view('templates/footer');
        } else {
			$data = [
				'judul' =>  $this->input->post('judul'),
				'genre' =>  $this->input->post('genre'),
				'rating' =>  $this->input->post('rating'),
				'durasi' =>  $this->input->post('durasi'),
				'score' =>  $this->input->post('score'),
				'rilis' =>  $this->input->post('rilis'),
				'sinopsis' =>  $this->input->post('sinopsis'),
				'gambar'       => substr(md5(rand(100000000, 200000000)), 0, 10)
			];
			$this->db->insert('film', $data);
			$foto = $this->db->get_where('film' , array('id_film' => $this->db->insert_id()))->row()->gambar;
			move_uploaded_file($_FILES['foto']['tmp_name'], 'assets/img/film/' . $foto . '.jpg');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Film berhasil ditambahkan </div>');
            redirect('admin/film');
        }
	}

	public function ubah_film($id='')
    { 
        $data['title'] = 'Data Film';
        $data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
        
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('genre', 'Genre', 'required');
        $this->form_validation->set_rules('rating', 'rating', 'required');
        $this->form_validation->set_rules('durasi', 'durasi', 'required');
        $this->form_validation->set_rules('score', 'score', 'required');
        $this->form_validation->set_rules('rilis', 'rilis', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Film gagal diubah</div>');
            redirect('admin/film');
        } else {
			$data = [
				'judul' =>  $this->input->post('judul'),
				'genre' =>  $this->input->post('genre'),
				'rating' =>  $this->input->post('rating'),
				'durasi' =>  $this->input->post('durasi'),
				'score' =>  $this->input->post('score'),
				'rilis' =>  $this->input->post('rilis'),
				'sinopsis' =>  $this->input->post('sinopsis'),
				'gambar'       => substr(md5(rand(100000000, 200000000)), 0, 10)
			];
			$this->db->where('id_film', $id);
			$this->db->update('film', $data);
			$foto = $data['gambar'];
			move_uploaded_file($_FILES['foto']['tmp_name'], 'assets/img/film/' . $foto . '.jpg');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Film berhasil diubah </div>');
            redirect('admin/film');
        }
	}

	public function hapus_film($id='')
	{
		$this->db->where('id_film', $id);
		$this->db->delete('film');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Film berhasil dihapus </div>');
		redirect('admin/film');
	}

	public function tiket()
    { 
        $data['title'] = 'Data Tiket';
		$data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
		$data['tiket'] = $this->db->query('SELECT tiket.`id_tiket`, tiket.`harga`, tiket.`stok`, film.`judul`, tiket.`id_film` FROM `tiket` JOIN `film` ON tiket.id_film = film.id_film order by tiket.id_tiket desc')->result();
        
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('stok', 'stok', 'required');
        $this->form_validation->set_rules('film', 'film', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/tiket', $data);
            $this->load->view('templates/footer');
        } else {
			$data = [
				'harga' =>  $this->input->post('harga'),
				'stok' =>  $this->input->post('stok'),
				'id_film' =>  $this->input->post('film')
			];
			$this->db->insert('tiket', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Tiket berhasil ditambahkan </div>');
            redirect('admin/tiket');
        }
	}
	
	public function ubah_tiket($id='')
    { 
        $data['title'] = 'Data Tiket';
		$data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
	   
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('stok', 'stok', 'required');
        $this->form_validation->set_rules('film', 'film', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Tiket gagal diubah</div>');
            redirect('admin/tiket');
        } else {
			$data = [
				'harga' =>  $this->input->post('harga'),
				'stok' =>  $this->input->post('stok'),
				'id_film' =>  $this->input->post('film')
			];
			$this->db->where('id_tiket', $id);
			$this->db->update('tiket', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Tiket berhasil diubah </div>');
            redirect('admin/tiket');
        }
	}


	public function hapus_tiket($id='')
	{
		$this->db->where('id_tiket', $id);
		$this->db->delete('tiket');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Tiket berhasil dihapus </div>');
		redirect('admin/tiket');
	}

	public function jadwal()
    { 
        $data['title'] = 'Data Jadwal';
        $data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
		$this->db->order_by('id_jadwal', 'desc');
		$data['jadwal'] = $this->db->get('jadwal')->result();
        
        $this->form_validation->set_rules('id_film', 'Film', 'required');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tgl_berhenti', 'Tanggal Berhenti', 'required');
        $this->form_validation->set_rules('id_sesi', 'Sesi', 'required');
        $this->form_validation->set_rules('id_ruang', 'Ruang', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/jadwal', $data);
            $this->load->view('templates/footer');
        } else {
			$data = [
				'id_film' =>  $this->input->post('id_film'),
				'tgl_mulai' =>  $this->input->post('tgl_mulai'),
				'tgl_berhenti' =>  $this->input->post('tgl_berhenti'),
				'id_sesi' =>  $this->input->post('id_sesi'),
				'id_ruang' =>  $this->input->post('id_ruang')
			];
			$this->db->insert('jadwal', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Film berhasil ditambahkan </div>');
            redirect('admin/jadwal');
        }
	}
	
	public function ubah_jadwal($id='')
    { 
        $data['title'] = 'Data Jadwal';
        $data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
        
        $this->form_validation->set_rules('id_film', 'Film', 'required');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tgl_berhenti', 'Tanggal Berhenti', 'required');
        $this->form_validation->set_rules('id_sesi', 'Sesi', 'required');
        $this->form_validation->set_rules('id_ruang', 'Ruang', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Jadwal gagal diubah</div>');
            redirect('admin/jadwal');
        } else {
			$data = [
				'id_film' =>  $this->input->post('id_film'),
				'tgl_mulai' =>  $this->input->post('tgl_mulai'),
				'tgl_berhenti' =>  $this->input->post('tgl_berhenti'),
				'id_sesi' =>  $this->input->post('id_sesi'),
				'id_ruang' =>  $this->input->post('id_ruang')
			];
			$this->db->where('id_jadwal', $id);
			$this->db->update('jadwal', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Film berhasil diubah </div>');
            redirect('admin/jadwal');
        }
	}


	public function hapus_jadwal($id='')
	{
		$this->db->where('id_jadwal', $id);
		$this->db->delete('jadwal');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Film berhasil dihapus </div>');
		redirect('admin/jadwal');
	}

	public function ruang()
    { 
        $data['title'] = 'Data Tiket';
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('username')])->row_array();
		$this->db->order_by('id_ruang', 'desc');
		$data['ruang'] = $this->db->get('ruang')->result();
        
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jm_kursi', 'Jumlah kursi', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/ruang', $data);
            $this->load->view('templates/footer');
        } else {
			$data = [
				'nama' =>  $this->input->post('nama'),
				'jm_kursi' =>  $this->input->post('jm_kursi')
			];
			$this->db->insert('ruang', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Ruang berhasil ditambahkan </div>');
            redirect('admin/ruang');
        }
	}
	public function ubah_ruang($id='')
    { 
        $data['title'] = 'Data Tiket';
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('username')])->row_array();
        
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jm_kursi', 'Jumlah kursi', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Ruang gagal diubah</div>');
            redirect('admin/ruang');
        } else {
			$data = [
				'nama' =>  $this->input->post('nama'),
				'jm_kursi' =>  $this->input->post('jm_kursi')
			];
			$this->db->where('id_raung', $id);
			$this->db->update('ruang', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Ruang berhasil diubah </div>');
            redirect('admin/ruang');
        }
	}
	public function hapus_ruang($id='')
	{
		$this->db->where('id_ruang', $id);
		$this->db->delete('ruang');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Ruang berhasil dihapus </div>');
		redirect('admin/ruang');
	}

	public function sesi()
    { 
        $data['title'] = 'Data Tiket';
		$data['user'] = $this->db->get_where('admin', ['username' =>$this->session->userdata('username')])->row_array();
		$this->db->order_by('id_sesi', 'asc');
		$data['sesi'] = $this->db->get('sesi')->result();
        
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/sesi', $data);
            $this->load->view('templates/footer');
    
	}

	public function pesanan()
    { 
        $data['title'] = 'Data Pesanan';
		$data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('username')])->row_array();
		$this->db->order_by('id_pemesan', 'desc');
		$data['pesanan'] = $this->db->get('pemesan')->result();

        $this->form_validation->set_rules('id_pesanan', 'Id Pesanan', 'required');
        $this->form_validation->set_rules('id_member', 'Member', 'required');
        $this->form_validation->set_rules('jml_tiket_pesan', 'Jumlah Tiket', 'required');
        $this->form_validation->set_rules('tgl_pesan', 'Tanggal Pesan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/pesanan', $data);
            $this->load->view('templates/footer');
        } else {
			$data = [
				'id_pemesan' =>  $this->input->post('id_pemesan'),
				'id_member' =>  $this->input->post('id_member'),
				'jml_tiket_pesan' =>  $this->input->post('jml_tiket_pesan'),
				'tgl_pesan' =>  $this->input->post('tgl_pesan'),
				'status' =>  $this->input->post('status')
			];
			$this->db->insert('pemesan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pesanan berhasil ditambahkan </div>');
            redirect('admin/pesanan');
        }
	}
	public function hapus_pesanan($id='')
	{
		$this->db->where('id_pemesan', $id);
		$this->db->delete('pemesan');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pesanan berhasil dihapus </div>');
		redirect('admin/pesanan');
	}

	public function proses_pesanan($id)
	{
		$this->db->where('id_pemesan', $id);
		$this->db->update('pemesan', ['status' => 1]);
		$this->session->set_flashdata('message', "<div class='alert alert-success' role='alert'>Pesanan berhasil dibayar</div>");
		redirect('admin/cetak/'. $id);
	}

	public function cetak($id)
	{
		$data['data'] = $this->db->query("SELECT * FROM film,tiket,dtl_pemesan, jadwal, sesi WHERE jadwal.id_sesi = sesi.id_sesi AND film.id_film = tiket.id_film AND tiket.id_tiket = dtl_pemesan.id_tiket AND jadwal.id_film = film.id_film AND dtl_pemesan.id_pemesan = '$id' GROUP BY film.id_film")->result_array();
		$this->load->view('admin/cetak', $data);
	}
}