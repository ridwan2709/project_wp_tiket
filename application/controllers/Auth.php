<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $error = validation_errors();
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $error . '</div>');
            redirect('welcome');
        } else {
            // jika validasi sukses
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('member', ['email' => $email])->row_array();
        $data = [
            'email' => $user['email'],
        ];
        // jika user ada
        if ($user) {
                // cek password
                    if (password_verify($password, $user['password'])) {
                    $this->session->set_userdata($data);
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil login! </div>');
                    redirect('welcome');
                } else {
                    $this->db->insert('member', $data);
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah! </div>');
                    redirect('welcome');
                }
        } else {

            $this->db->insert('member', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not register! </div>');
            redirect('welcome');
        }
    }

    public function registration()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[member.email]', [
            'is_unique' => 'This email has been register'
            ]);
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('pass1', 'Password', 'required|trim|min_length[3]|matches[pass2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password to sort!'
        ]);
        $this->form_validation->set_rules('pass2', 'Password', 'required|trim|matches[pass1]');
        if ($this->form_validation->run() == false) {
			$data['user'] = $this->db->get_where('member', ['email' =>$this->session->userdata('email')])->row_array();

			$error = validation_errors();
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $error . '</div>');
			redirect('welcome');
		} else {
            $data = [
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'foto' => '',
                'password' => password_hash($this->input->post('pass1'), PASSWORD_DEFAULT),
                'jk' => htmlspecialchars($this->input->post('jk', true)),
                'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true))
            ];

            $this->db->insert('member', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! your account has been created. Please login </div>');
            redirect('welcome');
        }
    }


    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('password');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil logout! </div>');
        redirect('welcome');
    }
}
