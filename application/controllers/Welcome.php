<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['user'] = $this->db->get_where('member', ['email' =>$this->session->userdata('email')])->row_array();

		$data['film'] = $this->db->get('film')->result_array();
		$data['pesanan'] = $this->db->query("SELECT MAX(id_pemesan) as kode FROM pemesan")->result_array();
	
		
		$this->load->view('frontend/temp_header', $data);
		$this->load->view('frontend/banner', $data);
		$this->load->view('frontend/home', $data);
		$this->load->view('frontend/temp_footer', $data);
	}
	
	public function detail_film($param="")
	{
		$data['user'] = $this->db->get_where('member', ['email' =>$this->session->userdata('email')])->row_array();
		$data['pesanan'] = $this->db->query("SELECT MAX(id_pemesan) as kode FROM pemesan")->result_array();
		$data['tampil'] = $this->db->query("SELECT * FROM film, tiket, jadwal, ruang WHERE tiket.id_film = film.id_film AND film.id_film = $param AND jadwal.id_film = film.id_film AND jadwal.id_ruang = ruang.id_ruang")->result_array();
		$data['jadwal'] = $this->db->get_where('jadwal', array('id_film' => $param))->result_array();

		
		$this->load->view('frontend/temp_header', $data);
		$this->load->view('frontend/detail_film', $data);
		$this->load->view('frontend/temp_footer', $data);


	}

	public function pesanan()
	{
		$this->form_validation->set_rules('kursi', 'Kursi', 'required');
		
		if ($this->form_validation->run() == false) {
			$data['user'] = $this->db->get_where('member', ['email' =>$this->session->userdata('email')])->row_array();
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kursi Harus diisi! </div>');
			redirect('welcome');
		}else{
			$id_tiket = $this->input->post('tiket');
			$kursi = $this->input->post('kursi');
			$cekdata = $this->db->query("SELECT kursi FROM dtl_pemesan,tiket WHERE tiket.id_tiket = $id_tiket AND dtl_pemesan.kursi = $kursi AND tiket.id_tiket = dtl_pemesan.id_tiket")->row();
			
			if ($cekdata) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kursi tidak tersedia! </div>');
				redirect('welcome');
			}else{
				$data = [
					'kursi' => htmlspecialchars($this->input->post('kursi', true)),
					'id_tiket' => htmlspecialchars($this->input->post('tiket', true)),
					'id_pemesan' => htmlspecialchars($this->input->post('pemesan', true)),
				];
				$this->db->insert('dtl_pemesan', $data);
				$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil pesan, Lihat Keranjang </div>');
				redirect('welcome');
			}

		}
	}

	public function hapus_pesan($id='')
	{
		$this->db->where('id_dtl_pemesan', $id);
		$this->db->delete('dtl_pemesan');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pesanan berhasil dihapus </div>');
		redirect('welcome');
	}

	public function proses_pesanan()
	{
		$data = [
				'id_pemesan' => htmlspecialchars($this->input->post('id_pemesan', true)),
				'id_member' => htmlspecialchars($this->input->post('id_member', true)),
				'jml_tiket_pesan' => htmlspecialchars($this->input->post('jm_tiket', true)),
				'total_harga' => htmlspecialchars($this->input->post('t_harga', true)),
				'tgl_pesan' => date('d-m-Y'),
				'status' => 0,
			];
			$this->db->insert('pemesan', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil pesan dengan ID Pemesan '. $this->input->post('id_pemesan'). '</div>');
			redirect('welcome');
		

	}
}
